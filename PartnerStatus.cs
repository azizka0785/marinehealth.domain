﻿namespace MarineHealth.Domain
{
    public enum PartnerStatus
    {
        BronzePartner,
        SilverPartner,
        GoldPartner,
        PlatinumPartner,
        DiamondPartner
    }
}
