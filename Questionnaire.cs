﻿namespace MarineHealth.Domain
{
    public class Questionnaire
    {
        public long Id { get; set; }

        public short Recommendable { get; set; }

        public short HighQualityService { get; set; }

        public short OrderPlacement { get; set; }

        public short ClearInformation { get; set; }

        public short DeliveryOrganization { get; set; }

        public short ResponseSpeed { get; set; }

        public short SupportQuality { get; set; }

        public short ProductQuality { get; set; }

        public short ProductAvailability { get; set; }

        public short OrderPayment { get; set; }

        public PartnerCategory PartnerCategory { get; set; } = new();
    }
}