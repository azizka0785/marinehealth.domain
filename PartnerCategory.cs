﻿namespace MarineHealth.Domain
{
    public class PartnerCategory
    {
        public long Id { get; set; }

        public string Name { get; set; } = "";

        public PartnerStatus Status { get; set; }

        public List<Questionnaire> Questionnaires { get; set; } = new();
    }
}
